public class Book {
    public String title;
    public String author;
    public int publishYear;

    public Book() {
    }

    public Book(String title, String author, int publishYear) {
        this.title = title;
        this.author = author;
        this.publishYear = publishYear;
    }

    public boolean isOlderThen(Book book) {
        return publishYear > book.publishYear;
    }

    public boolean isPublishAfter(int year) {
        return publishYear < year;
    }

    public void print() {
        System.out.println(String.format("Название книги: %s; Автор книги: %s; Год издания: %d", title, author, publishYear));
    }

}
