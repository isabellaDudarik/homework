public class BookShelf {
    Book[] books = Generator.generateBooks();

    public void findAndPrintOldestBook() {
        Book oldersBook = books[0];
        for (int i = 1; i < books.length; i++) {
            if (oldersBook.isOlderThen(books[i])) {
                oldersBook = books[i];
            }
        }
        System.out.println("Автор самой старой книги: " + oldersBook.author + "(" + oldersBook.publishYear + ")");
    }

    public void findAndPrintBooksNewerThan(int value) {
        for (Book book : books) {
            if (book.isPublishAfter(value)) {
                book.print();
            }
        }
    }
}
