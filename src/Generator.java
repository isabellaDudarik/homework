public class Generator {
    public static Book[] generateBooks() {
        Book[] books = new Book[5];

        books[0] = new Book("Женщина в белом", "Уилки Коллинз", 1860);

        books[1] = new Book("Цветок орхидеи", "Джеймс Хедли Чейз", 1948);

        books[2] = new Book("Брида", "Пауло Коэльо", 1990);

        books[3] = new Book("Милый друг", "Ги де Мопассан", 1885);

        books[4] = new Book("Королёк — птичка певчая", "Решат Нури Гюнтекин", 1922);

        return books;
    }
}
