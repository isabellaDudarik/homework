import java.util.Locale;
import java.util.Scanner;

public class Main_converter {
    public static void main(String[] args) {

        Bank bank[] = new Bank[3];
        bank[0] = new Bank();
        bank[0].banksname = "Privatbank";
        bank[0].array = new float[3];
        bank[0].array[0] = 26.7500f;
        bank[0].array[1] = 30.0000f;
        bank[0].array[2] = 0.3890f;

        bank[1] = new Bank();
        bank[1].banksname = "Оschadbank";
        bank[1].array = new float[3];
        bank[1].array[0] = 26.8000f;
        bank[1].array[1] = 29.9500f;
        bank[1].array[2] = 0.2600f;

        bank[2] = new Bank();
        bank[2].banksname = "Alphabank";
        bank[2].array = new float[3];
        bank[2].array[0] = 26.8500f;
        bank[2].array[1] = 30.1000f;
        bank[2].array[2] = 0.4100f;

        //init variables
        String nameCurrency[] = new String[3];
        nameCurrency[0] = "USD";
        nameCurrency[1] = "EUR";
        nameCurrency[2] = "RUB";

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter currency
        System.out.println("Enter the currency to convert (USD or EUR or RUB): ");
        String currency = scan.nextLine();

        System.out.println("Enter bank for conversion (Privatbank or Оschadbank or Alphabank): ");
        String bank1 = scan.nextLine();

        //convert uan to user currency
        for (int i = 0; i < bank.length; i++) {
            if (bank[i].banksname.equalsIgnoreCase(bank1)) {
                for (int j = 0; j < nameCurrency.length; j++) {
                    if (nameCurrency[j].equals(currency)) {
                        System.out.println(String.format(Locale.US, "You money in %s: %.2f", nameCurrency[j], (amount / bank[i].array[j])));
                    }
                }
            }
        }
    }
}

