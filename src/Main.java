import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BookShelf bookShelf = new BookShelf();
        bookShelf.findAndPrintOldestBook();

        Scanner in = new Scanner(System.in);
//        System.out.println("Введите автора: ");
//        String nameauthor = in.nextLine();
//        for (int i = 0; i < books.length; i++) {
//            if (books[i].author.equals(nameauthor)) {
//                System.out.println(books[i].title);
//            }
//        }
        System.out.println("Введите год издания книги: ");
        int year = Integer.valueOf(in.nextLine());
        bookShelf.findAndPrintBooksNewerThan(year);
    }
}
